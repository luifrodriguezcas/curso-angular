import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReservasListadoComponent } from './reservas-listado.component';



@NgModule({
  declarations: [ReservasListadoComponent],
  imports: [
    CommonModule
  ]
})
export class ReservasListadoModule { }
